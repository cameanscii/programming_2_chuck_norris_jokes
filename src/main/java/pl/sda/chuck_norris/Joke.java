package pl.sda.chuck_norris;

public class Joke {
    private String category = null;
    private String icon_url;
    private String id;
    private String url;
    private String value;


    // Getter Methods

    public String getCategory() {
        return category;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getValue() {
        return value;
    }

    // Setter Methods

    public void setCategory(String category) {
        this.category = category;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
