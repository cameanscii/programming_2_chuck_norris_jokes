package pl.sda.chuck_norris;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.xml.crypto.Data;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;
import java.net.URLConnection;

public class Main {
    public static void main(String[] args) throws IOException {


        for (int i = 0; i <10 ; i++) {

            URL chuckNorrisJoke = new URL("https://api.chucknorris.io/jokes/random");

            URLConnection connection = chuckNorrisJoke.openConnection();
            connection.setRequestProperty("User-agent", "Chrome");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                Joke joke = new Gson().fromJson(inputLine, Joke.class);
                System.out.println(joke.getValue());
            }
            in.close();
        }
    }
}
